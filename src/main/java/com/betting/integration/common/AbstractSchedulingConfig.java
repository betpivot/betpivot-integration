package com.betting.integration.common;

import org.quartz.*;

public class AbstractSchedulingConfig {
    protected JobDetail job(Class<? extends Job> jobClass, String jobName) {
        return JobBuilder.newJob(jobClass)
                .withIdentity(jobName)
                .storeDurably().build();
    }

    protected Trigger trigger(int everySeconds, JobDetail forJob, String identity) {
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(everySeconds).repeatForever();
        return TriggerBuilder.newTrigger().forJob(forJob)
                .withIdentity(identity).withSchedule(scheduleBuilder).build();
    }
}
