package com.betting.integration.betting.job.nike;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:betting-providers-jobs.properties")
@ConfigurationProperties(prefix = "nike")
@Getter
@Setter
public class NikeJobConfiguration {

    // FIXME add validation
    private String scrapeUrl;
    private String betTypeId;
    private String providerName;

    private String actionUrl;
}
