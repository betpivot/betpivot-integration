package com.betting.integration.betting.job.tipsport;

import com.betting.integration.betting.job.AbstractBettingProviderJob;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.PredictionMatch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TipsportJob extends AbstractBettingProviderJob {

    @Autowired
    private TipsportJobConfiguration conf;

    public void setConf(TipsportJobConfiguration conf) {
        this.conf = conf;
    }

    @Override
    protected Map<BettingProvider, List<BettingProviderMatch>> getData() throws Exception {
        Map<BettingProvider, List<BettingProviderMatch>> result = new HashMap<>();
        List<BettingProviderMatch> data = getSingleData();
        for (String providerName : conf.getProviderNames()) {
            BettingProvider provider = getProvider(providerName);
            result.put(provider, copy(data).stream()
                    .peek(m -> {
                        m.setProvider(provider);
                        m.getBets()
                                .forEach(b -> b.setBetLinkUrl(b.getBetLinkUrl().replace(conf.getDefaultBetUrlDomain(), providerName.toLowerCase())));
                    })
                    .collect(Collectors.toList()));
        }
        return result;
    }

    public List<BettingProviderMatch> copy(List<BettingProviderMatch> data) {
        return data.stream().map(this::copy).collect(Collectors.toList());
    }

    public BettingProviderMatch copy(BettingProviderMatch AnObject) {
        Gson gson = new GsonBuilder().create();
        String text = gson.toJson(AnObject);
        return gson.fromJson(text, BettingProviderMatch.class);
    }

    private List<BettingProviderMatch> getSingleData() throws IOException, XMLStreamException {
        List<BettingProviderMatch> result = new ArrayList<>();

        URL url = new URL(conf.getScrapeUrl());
        InputStream in = url.openStream();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser = factory.createXMLStreamReader(in);
        boolean readingEvent = false;
        boolean readingRelevantMatch = false;
        BettingProviderPlayerName name = null;
        BettingProviderMatch match = null;
        String matchId = null;
        String betlinkUrl = null;
        PredictionMatch predictionMatch = null;
        while (parser.hasNext()) {
            int event = parser.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                if ("match".equals(parser.getLocalName())) {
                    String[] matchName = parser.getAttributeValue(null, "name").split("-");
                    Optional<String> teamNameId1 = getTeamNameId(matchName[0]);
                    Optional<String> teamNameId2 = getTeamNameId(matchName[1]);
                    if (teamNameId1.isPresent() && teamNameId2.isPresent()) {
                        predictionMatch = getPredictionMatch(teamNameId1.get(), teamNameId2.get()).orElse(null);
                        if (predictionMatch != null) {
                            match = new BettingProviderMatch(teamNameId1.get(), teamNameId2.get(), null, predictionMatch.getExternalMatchId());
                            matchId = parser.getAttributeValue(null, "matchId");
                            betlinkUrl = parser.getAttributeValue(null, "url");
                            if (betlinkUrl.contains("?")) {
                                betlinkUrl = betlinkUrl + "&" + conf.getUrlSuffix();
                            } else {
                                betlinkUrl = betlinkUrl + "?" + conf.getUrlSuffix();
                            }
                            readingRelevantMatch = true;
                        }
                    }
                }
                if (readingRelevantMatch && "event".equals(parser.getLocalName()) && conf.getBetTypeId().equals(parser.getAttributeValue(null, "eventType"))) {
                    readingEvent = true;
                    String[] nameArr = parser.getAttributeValue(null, "name").split(" ")[2].split("\\.");
                    if (nameArr.length < 2) {
                        nameArr = Arrays.copyOfRange(parser.getAttributeValue(null, "name").split(" "), 2, 4);
                    }
                    if (nameArr.length > 2) {
                        nameArr = new String[] { nameArr[0], nameArr[nameArr.length - 1]};
                    }
                    name = new BettingProviderPlayerName(nameArr[0], nameArr[1]);
                }
                if (readingRelevantMatch && readingEvent && "odd".equals(parser.getLocalName())) {
                    Double rate = Double.valueOf(parser.getAttributeValue(null, "rate"));
                    Double line = Double.valueOf(parser.getAttributeValue(null, "fullName").split(" ")[2]);
                    BettingProviderBetType type = parser.getAttributeValue(null, "opportunityType").equals("u") ? BettingProviderBetType.LESS : BettingProviderBetType.MORE;

                    String eventId = parser.getAttributeValue(null, "eventId");
                    String opportunityId = parser.getAttributeValue(null, "opportunityId");
                    String betlinkUrlAddOnTicket = conf.getAddBetOnTicketTemplate()
                            .replace("{$EVENT_ID}", eventId)
                            .replace("{$OPPORTUNITY_ID}", opportunityId)
                            .replace("{$MATCH_ID}", matchId)
                            .replace("{$AMOUNT}", "100");
                    String externalId = getExternalId(name.getFirstName(), name.getLastName(), null, predictionMatch, String.join(", ", conf.getProviderNames()));
                    match.addBet(new BettingProviderBet(name, line, type, rate, betlinkUrl, null, externalId));
                }
            }
            if (event == XMLStreamConstants.END_ELEMENT) {
                if ("event".equals(parser.getLocalName())) {
                    readingEvent = false;
                }
                if ("match".equals(parser.getLocalName())) {
                    readingRelevantMatch = false;
                    betlinkUrl = null;
                    matchId = null;
                    if (match != null && match.getBets().size() > 0) {
                        result.add(match);
                        match = null;
                    }
                    predictionMatch = null;
                }
            }
        }

        return result;
    }
}
