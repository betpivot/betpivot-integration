package com.betting.integration.betting.job;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.prediction.PredictionTeam;
import com.betting.storage.data.suggestion.SuggestionMatch;
import com.betting.storage.data.suggestion.SuggestionState;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.betting.BettingProviderStorageService;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.service.push.PushNotificationService;
import com.betting.storage.service.suggestion.SuggestionIntegrationService;
import com.betting.storage.util.HashingService;
import lombok.Setter;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Setter
public abstract class AbstractBettingProviderJob extends QuartzJobBean {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private BettingProviderStorageService bettingStorage;
    @Autowired
    private PredictionStorageService predictionStorage;
    @Autowired
    private SuggestionIntegrationService suggestionService;
    @Autowired
    private PushNotificationService pushService;
    @Autowired
    private HashingService hashingService;

    @Override
    public void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        LOGGER.info("Executing job {}", getClass());
        LocalDateTime start = LocalDateTime.now();
        List<PredictionMatch> predictions = predictionStorage.getActivePredictions();
        if (predictions.isEmpty()) {
            LOGGER.info("No active predictions, skipping {}", getClass().getSimpleName());
            return;
        }
        ZonedDateTime utcNow = ZonedDateTime.now(ZoneOffset.UTC);
        if (predictions.stream().allMatch(m -> m.getStartTime().isBefore(utcNow.toLocalDateTime()))) {
            LOGGER.info("All predictions are playing or finished, skipping {}", getClass().getSimpleName());
            return;
        }

        try {
            Map<BettingProvider, List<BettingProviderMatch>> data = getData();
            if (data == null) {
                LOGGER.error("Cannot load data " + data + ", or provider" + data);
                throw new JobExecutionException("Cannot load data " + data + ", or provider" + data);
            }
            data.forEach((bp, matches) -> {
                List<BettingProviderMatch> oldMatches = bettingStorage.getMatches(bp);
                String providerId = hashingService.hash(bp.getId().toHexString());
                boolean matchesNotChanged =
                        matches.size() == oldMatches.size() &&
                        matches.stream()
                                .allMatch(m -> oldMatches.stream()
                                        .filter(om -> om.getExternalMatchId().equals(m.getExternalMatchId())).findAny()
                                        .map(om -> om.equals(m))
                                        .orElse(false));
                if (matchesNotChanged) {
                    LOGGER.info("[{}] [Notification] Not bets changed, skipping", getClass().getSimpleName());
                } else {
                    long deactivatedMatches = bettingStorage.saveData(matches, bp);
                    LOGGER.info("[{}] [Notification] Matches size {}", getClass().getSimpleName(), deactivatedMatches);
                    if (matches.size() > 0) {
                        List<SuggestionMatch> suggestions = suggestionService.calculateSuggestions();
                        if (deactivatedMatches == 0) {
                            boolean anyBetProfitable = suggestions.stream()
                                    .flatMap(m -> Stream.concat(
                                            m.getTeam1().getPlayers().stream().flatMap(suggestionPlayer -> suggestionPlayer.getSuggestions().stream()),
                                            m.getTeam2().getPlayers().stream().flatMap(suggestionPlayer -> suggestionPlayer.getSuggestions().stream())))
                                    .filter(s -> s.getProviderId().equalsIgnoreCase(providerId))
                                    .anyMatch(s -> SuggestionState.PROFITABLE.equals(s.getState()));
                            if (anyBetProfitable) {
                                LOGGER.info("[{}] [Notification] Notifying", getClass().getSimpleName());
                                pushService.notifyNewBets(bp);
                            } else {
                                LOGGER.info("[{}] [Notification] No bet profitable, skipping notification", getClass().getSimpleName());
                            }
                        } else {
                            LOGGER.info("[{}] [Notification] Found {} deactivated matches", getClass().getSimpleName(), deactivatedMatches);
                        }
                    } else {
                        LOGGER.info("[{}] [Notification] No matches found in provider", getClass().getSimpleName());
                    }
                }
            });
        } catch (Exception e) {
            Monitoring.businessError(Monitoring.BusinessObject.BET, "Cannot process provider import", "integration.bet.import.fail", getClass().getSimpleName(), getClass(), e);
            throw new JobExecutionException(e);
        }
        LOGGER.info("{} executed in {}", getClass().getSimpleName(), Duration.between(start, LocalDateTime.now()).toMillis());
    }

    protected BettingProvider getProvider(String name) {
        return bettingStorage.getActiveProvider(name).orElseThrow(() -> {
            LOGGER.error("Cannot find provider {}", name);
            return new IllegalStateException("Cannot find provider " + name);
        });
    }

    protected Optional<String> getTeamNameId(String name) {
        return predictionStorage.getTeamAny(name).map(PredictionTeam::getNameId);
    }

    protected String getExternalId(String firstName, String lastName, String teamNameId, PredictionMatch match, String logReportName) {
       return match.getPrediction(firstName, lastName, teamNameId, false, logReportName)
               .map(Prediction::getExternalId).orElse(null);
    }

    protected String getExternalId(String firstName, String lastName, String teamNameId, PredictionMatch match, boolean silently) {
        return match.getPrediction(firstName, lastName, teamNameId, silently, "silent mode")
                .map(Prediction::getExternalId).orElse(null);
    }

    protected Optional<PredictionMatch> getPredictionMatch(String team1NameId, String team2NameId) {
        List<PredictionMatch> predictions = predictionStorage
                .getActivePredictions().stream()
                .filter(p -> (p.getTeam1().getNameId().equals(team1NameId)
                        && p.getTeam2().getNameId().equals(team2NameId))
                || (p.getTeam2().getNameId().equals(team1NameId)
                && p.getTeam1().getNameId().equals(team2NameId)))
                .collect(Collectors.toList());
        if (predictions.size() == 0) {
            LOGGER.debug("No prediction match found for {} x {}", team1NameId, team2NameId);
            return Optional.empty();
        }
        if (predictions.size() > 1) {
            LOGGER.error("Found more active predictions for same teams {} x {}", team1NameId, team2NameId);
        }

        return Optional.of(predictions.get(0));
    }

    protected abstract Map<BettingProvider, List<BettingProviderMatch>> getData() throws Exception;
}
