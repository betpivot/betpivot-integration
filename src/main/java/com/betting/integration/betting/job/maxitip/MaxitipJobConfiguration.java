package com.betting.integration.betting.job.maxitip;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:betting-providers-jobs.properties")
@ConfigurationProperties(prefix = "maxitip")
@Getter
@Setter
public class MaxitipJobConfiguration {

    // FIXME add validation
    private String scrapeUrl;
    private String betTypeId;
    private String providerName;
    private Boolean skipNameCheck;
    private String betLinkTemplate;

}
