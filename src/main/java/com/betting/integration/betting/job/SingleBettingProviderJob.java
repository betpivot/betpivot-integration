package com.betting.integration.betting.job;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;
import com.betting.storage.data.prediction.PredictionMatch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SingleBettingProviderJob extends AbstractBettingProviderJob {

    @Override
    protected Map<BettingProvider, List<BettingProviderMatch>> getData() throws Exception {
        Map<BettingProvider, List<BettingProviderMatch>> result = new HashMap<>();
        BettingProvider provider = getProvider(getProviderName());
        result.put(provider, getSingleData(provider));
        return result;
    }

    protected abstract List<BettingProviderMatch> getSingleData(BettingProvider provider) throws Exception;

    protected abstract String getProviderName();

    protected String getExternalId(String firstName, String lastName, String teamNameId, PredictionMatch match) {
        return super.getExternalId(firstName, lastName, teamNameId, match, getProviderName());
    }
}
