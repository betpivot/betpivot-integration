package com.betting.integration.betting.job.fortuna;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Configuration
@PropertySource("classpath:betting-providers-jobs.properties")
@ConfigurationProperties(prefix = "fortuna")
@Getter
@Setter
public class FortunaJobConfiguration {

    // FIXME add validation
    private String scrapeUrl;
    private String betTypeId;
    private String providerName;
    private String actionBaseUrl;
    private String deepLinkTemplate;

}
