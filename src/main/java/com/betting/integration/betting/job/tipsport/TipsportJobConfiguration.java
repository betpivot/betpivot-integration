package com.betting.integration.betting.job.tipsport;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@PropertySource("classpath:betting-providers-jobs.properties")
@ConfigurationProperties(prefix = "tipsport")
@Getter
@Setter
public class TipsportJobConfiguration {

    // FIXME add validation
    private String scrapeUrl;
    private String betTypeId;
    private String defaultBetUrlDomain;
    private String addBetOnTicketTemplate;
    private String urlSuffix;
    private List<String> providerNames;

}
