package com.betting.integration.betting.job.sazka;

import com.betting.integration.betting.job.SingleBettingProviderJob;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.fail.BusinessFail;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import lombok.Setter;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.empty;

@Component
public class SazkaJob extends SingleBettingProviderJob {



    @Autowired
    @Setter
    private SazkaJobConfiguration conf;



    @Override
    protected List<BettingProviderMatch> getSingleData(BettingProvider provider) throws Exception {
        try (WebClient webClient = new WebClient()) {
            webClient.setAjaxController(new NicelyResynchronizingAjaxController());
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setDownloadImages(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.setJavaScriptTimeout(30000);
            webClient.waitForBackgroundJavaScript(10 * 60 * 1000);
            HtmlPage page = webClient.getPage(conf.getScrapeUrl());
            waitForPage(page);
            return page.querySelectorAll(".event-all-bets-inner").stream()
                    .filter(link -> link.getAttributes().getNamedItem("href").getNodeValue().startsWith("/basketbal"))
                    .map(link -> getBettingMatch(webClient, provider, page, (HtmlElement) link))
                    .filter(Optional::isPresent).map(Optional::get)
                    .collect(Collectors.toList());
        }
    }

    private Optional<BettingProviderMatch> getBettingMatch(WebClient webClient, BettingProvider provider, HtmlPage nbaPage, HtmlElement link){
        URL href = null;
        String oldHref = link.getAttributes().getNamedItem("href").getNodeValue();
        HtmlElement newLink = (HtmlElement) nbaPage.querySelectorAll("a[href='" + oldHref + "']").stream()
                .filter(e -> e.getAttributes().getNamedItem("data-action").getNodeValue().equalsIgnoreCase("Application.OpenGame"))
                .findAny().orElse(null);
        try {
            href = nbaPage.getFullyQualifiedUrl(oldHref);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("[Sazka] Navigating to " + href);
        HtmlPage matchPage = nbaPage;
        if (newLink == null) {
            LOGGER.error("New link was not found in page. Match {} could not be parsed", href);
            return Optional.empty();
        }
        try {
            newLink.click();
        } catch (IOException e) {
            LOGGER.error("Cannot process sazka import because of IO", e);
            return Optional.empty();
        }
//        HtmlPage matchPage;
//        try {
//            matchPage = webClient.getPage(href);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        waitForPage(matchPage);
        LOGGER.info("[Sazka] Navigated");
        DomNodeList<DomNode> nameNode = matchPage.querySelectorAll(".breadcrumb-last .breadcrumb-text");
        if (nameNode.isEmpty()) {
            LOGGER.error("Empty match");
            return empty();
        }
        DomNode teamsNode = nameNode.get(0);
        String[] names = teamsNode.getTextContent().split("&nbspvs&nbsp");
        if (names.length < 2) {
            return empty();
        }
        LOGGER.info("[Sazka] Teams: " + names[0].trim() + " vs "+ names[1].trim());
        Optional<String> teamNameId1 = getTeamNameId(names[0].trim());
        Optional<String> teamNameId2 = getTeamNameId(names[1].trim());
        if ((!teamNameId1.isPresent() || !teamNameId2.isPresent()) && (conf.getSkipNameCheck() == null || !conf.getSkipNameCheck())) {
            LOGGER.info("[Sazka] Team name is not present: " + teamNameId1 + ", "+ teamNameId2 + ", " + String.valueOf(conf.getSkipNameCheck()));
            goBackToNba(matchPage, teamsNode);
            return empty();
        }
        Optional<PredictionMatch> predictionMatch = getPredictionMatch(teamNameId1.get(), teamNameId2.get());
        if (!predictionMatch.isPresent()) {
            goBackToNba(matchPage, teamsNode);
            return empty();
        }
//        String dataid;
//        try {
//            dataid = URLEncoder.encode(matchPage.querySelectorAll(".games .light_gray_bg").get(0).getAttributes().getNamedItem("dataid").getTextContent(), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            throw new RuntimeException(e);
//        }
        //String betlinkUrl = conf.getAddBetOnTicketTemplate().replace("{$ITEM_ID}", dataid);
        String betlinkUrl = href.toString().replace("rsb.sazka.cz", "m.sazka.cz/kurzove-sazky");
        BettingProviderMatch match = new BettingProviderMatch(teamNameId1.orElse(""), teamNameId2.orElse(""), provider, predictionMatch.get().getExternalMatchId());
        LOGGER.info("[Sazka] Created match");
        matchPage.cleanUp();
        List<DomNode> betList = matchPage.querySelectorAll(".branch_header_class_2").stream()
                .filter(s -> {
                    String betType = conf.getBetTypeId();
                    String typeEleContent = s.getTextContent();
                    String[] split = typeEleContent.split(" ");
                    return split[split.length - 1].equalsIgnoreCase(betType);
                }).flatMap(e -> e.getParentNode()
                        .querySelectorAll(".bet-button").stream()).collect(Collectors.toList());
        LOGGER.info("[Sazka] Bet list element list: {}", betList.size());

        List<BettingProviderBet> bets = betList.stream()
                .map(dl -> processOneBet(betlinkUrl, dl, predictionMatch.get()))
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
        LOGGER.info("[Sazka] Converted to bets: {}", bets.size());

        match.getBets().addAll(bets);
        goBackToNba(matchPage, teamsNode);

        return Optional.of(match);
    }

    private void goBackToNba(HtmlPage matchPage, DomNode teamsNode) {
        HtmlElement nbaNode = (HtmlElement) teamsNode.getParentNode().getParentNode().querySelectorAll(".breadcrumb-text").get(1);
        try {
            nbaNode.click();
        } catch (IOException e) {
            try {
                nbaNode.click();
            } catch (IOException e1) {
                LOGGER.error("Failed to click", e, e1);
                throw new RuntimeException(e1);
            }
            LOGGER.error("Failed to click", e);
        }
        waitForPage(matchPage);
    }

    private Optional<BettingProviderBet> processOneBet(String betlinkUrl, DomNode dl, PredictionMatch predictionMatch) {
        LOGGER.info("Processing {}", dl.getTextContent());
        DomNodeList<DomNode> betDescription = dl.querySelectorAll(".bet-description .bet-title-team-name");
        DomNodeList<DomNode> betRate = dl.querySelectorAll(".bet-odds .bet-odds-number");
        if (betDescription.isEmpty() || betRate.isEmpty()) {
            LOGGER.error("Cannot parse descp {}, rate {}", betDescription.size(), betRate.size());
            return Optional.empty();
        }

        String[] bet = betDescription.get(0).getTextContent().split(" [-–] ");
        if (bet.length < 2) {
            LOGGER.error("Could not parse {}", betDescription.get(0).getTextContent());
            return Optional.empty();
        }
        String[] textLine = bet[bet.length - 1].split(" ");
        String[] namesArr = bet[0].replaceAll(": " + conf.getBetTypeId(), "").split(" ");
        Double line;
        try {
            line = Double.valueOf(textLine[textLine.length - 1].replace(",", "."));
        } catch (Exception e) {
            LOGGER.error("Cannot parse LINE for SazkaBet", e);
            return Optional.empty();
        }
        String underOverText = bet[bet.length - 1];
        BettingProviderBetType type = underOverText.contains("pod ") || underOverText.contains("Under ")
                ? BettingProviderBetType.LESS
                :  underOverText.contains("nad ") || underOverText.contains("Over ")
                    ? BettingProviderBetType.MORE
                    : null;
        if (type == null) {
            LOGGER.error("Could not determine over / under for data {}, element {}", bet, betDescription.get(0).getTextContent());
            return Optional.empty();
        }
        Double rate = Double.valueOf(betRate.get(0).getTextContent().replace(",", "."));

        BettingProviderPlayerName playerName = new BettingProviderPlayerName(namesArr[0], namesArr[namesArr.length - 1]);
        String externalId = getExternalId(playerName.getFirstName(), playerName.getLastName(), null, predictionMatch, true);
        if (externalId == null && namesArr.length >= 2) {
            playerName = new BettingProviderPlayerName(namesArr[0], namesArr[1]);
            externalId = getExternalId(playerName.getFirstName(), playerName.getLastName(), null, predictionMatch);
        }
        BettingProviderBet betObject = new BettingProviderBet(playerName, line, type, rate, betlinkUrl, null, externalId);
        LOGGER.info("Resolved to {}", betObject);
        return Optional.of(betObject);
    }

    private void waitForPage(HtmlPage page) {
//        int tries = 10;  // Amount of tries to avoid infinite loop
//        boolean rdy = false;
//        while (tries > 0 && !rdy) {
//            tries--;
//            synchronized(page) {
//                try {
//                    page.wait(1000);
//                    //rdy = page.querySelectorAll(".events-wrapper-container").getLength() > 0;
//                    page.wait(1000);
//                } catch (InterruptedException e) {
//                    Thread.currentThread().interrupt();
//                }
//            }
//        }
    }

    @Override
    protected String getProviderName() {
        return conf.getProviderName();
    }
}
