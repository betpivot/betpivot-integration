package com.betting.integration.betting.job.maxitip;

import com.betting.integration.betting.job.SingleBettingProviderJob;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.PredictionMatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MaxitipJob extends SingleBettingProviderJob {

    @Autowired
    private MaxitipJobConfiguration conf;

    public void setConf(MaxitipJobConfiguration conf) {
        this.conf = conf;
    }

    @Override
    protected List<BettingProviderMatch> getSingleData(BettingProvider provider) throws Exception {
        return getDocument(conf.getScrapeUrl(), 0)
                .select(".support_bets a:not(.statistics_match)").stream()
                .map(e -> loadBetsFromActionPage(provider, e.attr("href")))
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    protected String getProviderName() {
        return conf.getProviderName();
    }

    private Optional<BettingProviderMatch> loadBetsFromActionPage(BettingProvider provider, String actionPageUrl) {
        LOGGER.debug("[Maxi] Navigating to: " + actionPageUrl);
        Document actionPage = getDocument(actionPageUrl, 0);

        String[] names = actionPage.select(".shadow_box.support_bets_offer h2 a").text().replace(" ", "").split("-");
        if (names.length < 2) {
            LOGGER.debug("[Maxi] Names not long enough: " + Arrays.asList(names));
            return Optional.empty();
        }
        Optional<String> teamNameId1 = getTeamNameId(names[0]);
        Optional<String> teamNameId2 = getTeamNameId(names[1]);
        if ((!teamNameId1.isPresent() || !teamNameId2.isPresent()) && (conf.getSkipNameCheck() == null || !conf.getSkipNameCheck())) {
            LOGGER.debug("[Maxi] Team name is not present: " + teamNameId1 + ", "+ teamNameId2 + ", " + String.valueOf(conf.getSkipNameCheck()));
            return Optional.empty();
        }
        Optional<PredictionMatch> predictionMatch = getPredictionMatch(teamNameId1.get(), teamNameId2.get());
        if (!predictionMatch.isPresent()) {
            return Optional.empty();
        }
        BettingProviderMatch match = new BettingProviderMatch(teamNameId1.orElse(names[0]), teamNameId2.orElse(names[1]), provider, predictionMatch.get().getExternalMatchId());

        List<BettingProviderBet> bets = actionPage.select(".match .match-odds .odd.bet").stream()
                .filter(e -> e.attributes().get("onclick").contains(asciiEncoded(conf.getBetTypeId())))
                .flatMap(e -> createBet(actionPage, e.parent().parent().parent().parent().parent().parent(), predictionMatch.get()).stream())
                .collect(Collectors.toList());
        match.getBets().addAll(bets);
        LOGGER.debug("[Maxi] Created: " + match);
        return Optional.of(match);
    }

    private String asciiEncoded(String utf) {
        final StringBuilder out = new StringBuilder();
        for (int i = 0; i < utf.length(); i++) {
            final char ch = utf.charAt(i);
            if (ch <= 127) out.append(ch);
            else out.append("\\u").append(String.format("%04x", (int)ch));
        }

        return out.toString();
    }

    private List<BettingProviderBet> createBet(Document actionPage, Element row, PredictionMatch predictionMatch) {
        LOGGER.debug("[Maxi] Creating bet for row {}", row.text());
        ArrayList<BettingProviderBet> result = new ArrayList<>();
        String boxName = row.select(".matchName").text();
        String[] nameBoxArrBySpace = boxName.split(" ");
        String[] nameBoxArrByBracket = boxName.split("\\(")[0].split(" ");
        String teamNameId = nameBoxArrBySpace[nameBoxArrBySpace.length - 2].replace("(", "").replace(")", "").toUpperCase();
        String resolvedTeamNameId = getTeamNameId(teamNameId).orElse(null);
        Double line = Double.valueOf(nameBoxArrBySpace[nameBoxArrBySpace.length - 1].replace("(", "").replace(")", ""));
        Elements rates = row.select(".match .match-odds .odd.bet span");
        if (rates.size() < 2) {
            LOGGER.debug("[Maxi] Not enough rates: " + rates);
            return Collections.emptyList();
        }

        String pageUrl = actionPage.location();
        Map<String, List<String>> query = null;
        try {
            query = splitQuery(new URL(pageUrl));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        String betLinkUrl = conf.getBetLinkTemplate()
                .replace("{1}", query.get("sport").get(0))
                .replace("{2}", query.get("region").get(0))
                .replace("{3}", query.get("league").get(0))
                .replace("{4}", query.get("oppty").get(0));
        Double rateLess = Double.valueOf(rates.get(0).text());
        Double rateMore = Double.valueOf(rates.get(1).text());
        String firstName = nameBoxArrByBracket[nameBoxArrByBracket.length - 1].replaceAll(" ", "");
        String lastName = nameBoxArrByBracket[0].replaceAll(" ", "");
        BettingProviderPlayerName playerName = new BettingProviderPlayerName(firstName, lastName);
        String externalId = getExternalId(playerName.getFirstName(), playerName.getLastName(), resolvedTeamNameId, predictionMatch);
        if (externalId == null && firstName.length() > 2) {
            externalId = getExternalId(playerName.getLastName(), playerName.getFirstName(), resolvedTeamNameId, predictionMatch);
            if (externalId != null) {
                playerName.setFirstName(lastName);
                playerName.setLastName(firstName);
            }
        }
        BettingProviderBet betLess = new BettingProviderBet(playerName, line, BettingProviderBetType.LESS, rateLess, betLinkUrl, null, externalId);
        LOGGER.debug("Created {}", betLess);
        BettingProviderBet betMore = new BettingProviderBet(playerName, line, BettingProviderBetType.MORE, rateMore, betLinkUrl, null, externalId);
        LOGGER.debug("Created {}", betMore);
        result.add(betLess);
        result.add(betMore);

        return result;
    }

    public static Map<String, List<String>> splitQuery(URL url) throws UnsupportedEncodingException {
        final Map<String, List<String>> query_pairs = new LinkedHashMap<>();
        final String[] pairs = url.getQuery().split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<String>());
            }
            final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }

    private Document getDocument(String url, int tries) {
        if (tries >= 10) {
            throw new RuntimeException("Maximum tries to reach server");
        }
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            LOGGER.warn("Cannot connect to server", e);
            return getDocument(url, tries++);
        }
    }
}
