package com.betting.integration.betting.job.fortuna;

import com.betting.integration.betting.job.SingleBettingProviderJob;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.PredictionMatch;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Setter
public class FortunaJob extends SingleBettingProviderJob {

    @Autowired
    private FortunaJobConfiguration conf;

    @Override
    protected List<BettingProviderMatch> getSingleData(BettingProvider provider) throws Exception {
        return getDocument(conf.getScrapeUrl(), 0)
                .select(".bet_item_detail_href").stream()
                .map(el -> {
                    try {
                        return loadBetsFromActionPage(provider, el.attr("abs:href"));
                    } catch (UnsupportedEncodingException e) {
                        throw new IllegalStateException(e);
                    }
                })
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());


    }

    @Override
    protected String getProviderName() {
        return conf.getProviderName();
    }

    private Optional<BettingProviderMatch> loadBetsFromActionPage(BettingProvider provider, String actionPageUrl) throws UnsupportedEncodingException {
        LOGGER.info("Navigating to {}", actionPageUrl);
        Document actionPage = getDocument(actionPageUrl, 0);
        String actionId = actionPage.select("#page-variables").attr("data-action");
        String[] betName = actionPage.select(".bet_table_title").text().split(" \\| ");
        if (betName.length < 3) {
            LOGGER.info("{} Bet name not parsed {}", actionPageUrl, betName);
            return Optional.empty();
        }
        String[] names = betName[2].split(" - ");
        Optional<String> teamNameId1 = getTeamNameId(names[0]);
        Optional<String> teamNameId2 = getTeamNameId(names[1]);
        if (!teamNameId1.isPresent() || !teamNameId2.isPresent()) {
            return Optional.empty();
        }
        Optional<PredictionMatch> predictionMatch = getPredictionMatch(teamNameId1.get(), teamNameId2.get());
        if (!predictionMatch.isPresent()) {
            return Optional.empty();
        }
        String[] url = actionPageUrl.split("-");
        String actionUrl = conf.getActionBaseUrl() + URLEncoder.encode(names[0] + "-" + names[1] + "-MCZ" + url[url.length - 1], "UTF-8");
        BettingProviderMatch match = new BettingProviderMatch(teamNameId1.get(), teamNameId2.get(), provider, predictionMatch.get().getExternalMatchId());
        List<BettingProviderBet> bets = actionPage.select("#bet_table_holder-" + actionId + "-" + conf.getBetTypeId() + " tbody tr").stream()
                .flatMap(row -> createBet(row, actionUrl, predictionMatch.get())).collect(Collectors.toList());
        if (bets.isEmpty()) {
            LOGGER.info("{} Not bets found", actionPageUrl);
            return Optional.empty();
        }
        match.getBets().addAll(bets);
        return Optional.of(match);
    }

    private Stream<BettingProviderBet> createBet(Element row, String actionUrl, PredictionMatch predictionMatch) {
        String[] betName = row.select(".bet_item_detail_href").text().split(":")[1].split(" ");
        LOGGER.debug("Processing {}", Arrays.asList(betName));
        String[] names = new String[] { betName[0], betName[1] };
        Double line = Double.valueOf(betName[betName.length - 1].replace("(", "").replace(")", ""));
        Elements betLinkEl = row.select(".add_bet_link");
        BettingProviderBet less = null;
        if (betLinkEl.size() > 0) {
            less = createTip(predictionMatch, actionUrl, names, line, betLinkEl.get(0), BettingProviderBetType.LESS);
        }
        BettingProviderBet more = null;
        if (betLinkEl.size() > 1) {
             more = createTip(predictionMatch, actionUrl, names, line, betLinkEl.get(1), BettingProviderBetType.MORE);
        }
        return Stream.of(less, more);
    }

    private BettingProviderBet createTip(PredictionMatch predictionMatch, String actionUrl, String[] names, Double line, Element betLink, BettingProviderBetType type) {
        Double rate = Double.valueOf(betLink.text());
        String info = betLink.attr("data-info");
        String tipId = betLink.attr("data-odd");
        String deepLink = conf.getDeepLinkTemplate().replace("{$INFO}", info).replace("{$TIP_ID}", tipId).replace("{$RATE}", betLink.text());
        BettingProviderPlayerName playerName = new BettingProviderPlayerName(names[1], names[0]);
        String externalId = getExternalId(playerName.getFirstName(), playerName.getLastName(), null, predictionMatch);
        return new BettingProviderBet(playerName, line, type, rate, actionUrl, deepLink, externalId);
    }

    private Document getDocument(String url, int tries) {
        if (tries >= 10) {
            throw new RuntimeException("Maximum tries to reach server");
        }
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            LOGGER.warn("Cannot connect to server", e);
            return getDocument(url, tries++);
        }
    }
}
