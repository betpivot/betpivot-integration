package com.betting.integration.betting.job.nike;

import com.betting.integration.betting.job.SingleBettingProviderJob;
import com.betting.storage.data.betting.*;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.data.prediction.PredictionMatch;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class NikeJob extends SingleBettingProviderJob {

    @Autowired
    private NikeJobConfiguration conf;

    public void setConf(NikeJobConfiguration conf) {
        this.conf = conf;
    }

    @Override
    protected List<BettingProviderMatch> getSingleData(BettingProvider provider) throws Exception {

        try (WebClient webClient = new WebClient(BrowserVersion.EDGE)) {
            webClient.setAjaxController(new NicelyResynchronizingAjaxController());
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.setJavaScriptTimeout(30000);

            HtmlPage page = webClient.getPage(conf.getScrapeUrl());
            waitForPage(page);
            return page.querySelectorAll(".odd-bet-number").stream()
                    .map(link -> getBettingMatch(provider, page, (DomElement) link))
                    .filter(Optional::isPresent).map(Optional::get)
                    .collect(Collectors.toList());
        }
    }

    private Optional<BettingProviderMatch> getBettingMatch(BettingProvider provider, HtmlPage page, DomElement link){
        try {
            link.click();
        } catch (IOException e) {
            log.error("[Nike] Cannot click on bet offer", e);
        }
        waitForPage(page);
        DomNodeList<DomNode> names = page.querySelectorAll(".reakt-scoreboard-opponent");
        String team1 = names.get(0).getTextContent();
        String team2 = names.get(0).getTextContent();
        Optional<PredictionMatch> predictionMatch = getPredictionMatch(team1, team2);
        if (!predictionMatch.isPresent()) {
            return Optional.empty();
        }

        BettingProviderMatch bettingProviderMatch = new BettingProviderMatch(predictionMatch.get().getTeam1().getNameId(), predictionMatch.get().getTeam2().getNameId(), provider, predictionMatch.get().getExternalMatchId());

        DomNode betType = page.getFirstByXPath("//*[contains(text(),'" + conf.getBetTypeId() + "')]");
        DomNode parentOfRelevantBets = betType.getParentNode().getParentNode().getParentNode();

        List<BettingProviderBet> bets = parentOfRelevantBets.querySelectorAll(".bets-row").stream()
                .flatMap(dn -> processOneBet(dn, predictionMatch.get()))
                .collect(Collectors.toList());

        bettingProviderMatch.setBets(bets);

        return Optional.of(bettingProviderMatch);
    }

    private Stream<BettingProviderBet> processOneBet(DomNode dl, PredictionMatch predictionMatch) {
        String[] playerName = dl.querySelectorAll(".bet-box-title").get(0).getTextContent().split(" ");
        DomNodeList<DomNode> bets = dl.querySelectorAll("a.bet-box");

        DomNode nodeLess = bets.get(0);
        Double lessLine = Double.valueOf(nodeLess.querySelectorAll(".bet-left").get(0).getTextContent().split(" ")[2]);
        Double lessRate = Double.valueOf(nodeLess.querySelectorAll(".bet-right").get(0).getTextContent());

        DomNode nodeMore = bets.get(1);
        Double moreLine = Double.valueOf(nodeMore.querySelectorAll(".bet-left").get(0).getTextContent().split(" ")[2]);
        Double moreRate = Double.valueOf(nodeMore.querySelectorAll(".bet-right").get(0).getTextContent());

        BettingProviderPlayerName bettingProviderPlayerName = new BettingProviderPlayerName(playerName[1], playerName[0]);

        String externalId = getExternalId(bettingProviderPlayerName.getFirstName(), bettingProviderPlayerName.getLastName(), null, predictionMatch);

        return Stream.of(
                new BettingProviderBet(bettingProviderPlayerName, lessLine, BettingProviderBetType.LESS, lessRate, conf.getActionUrl(), null, externalId),
                new BettingProviderBet(bettingProviderPlayerName, moreLine, BettingProviderBetType.MORE, moreRate, conf.getActionUrl(), null, externalId));
    }

    private void waitForPage(HtmlPage page) {
        int tries = 10;  // Amount of tries to avoid infinite loop
        boolean loaded = false;
        while (tries > 0 && !loaded) {
            tries--;
            synchronized(page) {
                try {
                    page.wait(2000);  // How
                    loaded = page.querySelectorAll(".reakt-scoreboard-score-row").getLength() > 0;
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    @Override
    protected String getProviderName() {
        return conf.getProviderName();
    }
}
