package com.betting.integration.task;

import com.betting.storage.service.suggestion.SuggestionStorageService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

@Component
@Slf4j
public class ClearOldSuggestionsJob extends QuartzJobBean {

    private final SuggestionStorageService suggestionStorage;

    @Autowired
    public ClearOldSuggestionsJob(SuggestionStorageService suggestionStorage) {
        this.suggestionStorage = suggestionStorage;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("Running clear old suggestions job");
        LocalDateTime oneWeekAgo = LocalDateTime.now(ZoneOffset.UTC)
                .minus(2, ChronoUnit.WEEKS);
        log.info("Deleting suggestions older then {}", oneWeekAgo);
        long deletedCount = suggestionStorage.delete(oneWeekAgo);
        log.info("Delete {} suggestion matches", deletedCount);
    }
}
