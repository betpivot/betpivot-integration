package com.betting.integration.configuration;

import com.betting.integration.betting.job.fortuna.FortunaJob;
import com.betting.integration.betting.job.maxitip.MaxitipJob;
import com.betting.integration.betting.job.nike.NikeJob;
import com.betting.integration.betting.job.sazka.SazkaJob;
import com.betting.integration.betting.job.tipsport.TipsportJob;
import com.betting.integration.common.AbstractSchedulingConfig;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BettingProviderSchedulingConfig extends AbstractSchedulingConfig {

    @Bean
    @ConditionalOnProperty("scheduling.fortuna.enabled")
    public JobDetail fortunaJob() {
            return job(FortunaJob.class, "fortuna-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.fortuna.enabled")
    public Trigger fortunaTrigger(@Value("${scheduling.fortuna.every-seconds}") int everySeconds) {
        return trigger(everySeconds, fortunaJob(), "fortuna-trigger");
    }

    @Bean
    @ConditionalOnProperty("scheduling.tipsport.enabled")
    public JobDetail tipsportJob() {
        return job(TipsportJob.class, "tipsort-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.tipsport.enabled")
    public Trigger tipsportTrigger(@Value("${scheduling.tipsport.every-seconds}") int everySeconds) {
        return trigger(everySeconds, tipsportJob(), "tipsport-trigger");
    }

    @Bean
    @ConditionalOnProperty("scheduling.sazka.enabled")
    public JobDetail sazkaJob() {
        return job(SazkaJob.class, "sazka-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.sazka.enabled")
    public Trigger sazkaTrigger(@Value("${scheduling.sazka.every-seconds}") int everySeconds) {
        return trigger(everySeconds, sazkaJob(), "sazka-trigger");
    }

    @Bean
    @ConditionalOnProperty("scheduling.maxitip.enabled")
    public JobDetail maxitipJob() {
        return job(MaxitipJob.class, "maxitip-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.maxitip.enabled")
    public Trigger maxitipTrigger(@Value("${scheduling.maxitip.every-seconds}") int everySeconds) {
        return trigger(everySeconds, maxitipJob(), "maxitip-trigger");
    }

    @Bean
    @ConditionalOnProperty("scheduling.nike.enabled")
    public JobDetail nikeJob() {
        return job(NikeJob.class, "nike-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.nike.enabled")
    public Trigger nikeTrigger(@Value("${scheduling.nike.every-seconds}") int everySeconds) {
        return trigger(everySeconds, nikeJob(), "nike-trigger");
    }

}
