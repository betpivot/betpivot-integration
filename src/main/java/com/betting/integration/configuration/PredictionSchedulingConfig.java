package com.betting.integration.configuration;

import com.betting.integration.common.AbstractSchedulingConfig;
import com.betting.integration.prediction.job.PredictionJob;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PredictionSchedulingConfig extends AbstractSchedulingConfig {

    @Bean
    @ConditionalOnProperty("scheduling.prediction.enabled")
    public JobDetail predictionJob() {
            return job(PredictionJob.class, "suggestion-import");
    }

    @Bean
    @ConditionalOnProperty("scheduling.prediction.enabled")
    public Trigger predictionTrigger(@Value("${scheduling.fortuna.every-seconds}") int everySeconds) {
        return trigger(everySeconds, predictionJob(), "suggestion-trigger");
    }
}
