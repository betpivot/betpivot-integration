package com.betting.integration.configuration;

import com.betting.integration.common.AbstractSchedulingConfig;
import com.betting.integration.task.ClearOldSuggestionsJob;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.time.ZoneOffset;
import java.util.TimeZone;

@Configuration
@PropertySource("classpath:betting-providers-jobs.properties")
public class TaskSchedulingConfig extends AbstractSchedulingConfig {

    @Bean
    @ConditionalOnProperty("scheduling.task.clearOldSuggestions.enabled")
    public JobDetail clearOldSuggestionsJobs() {
        return job(ClearOldSuggestionsJob.class, "clear-old-suggestions-job");
    }

    @Bean
    @ConditionalOnProperty("scheduling.task.clearOldSuggestions.enabled")
    public Trigger clearOldSuggestionsTrigger() {
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder
                .dailyAtHourAndMinute(4, 0)
                .inTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));

        return TriggerBuilder.newTrigger().forJob(clearOldSuggestionsJobs())
                .withIdentity("clear-old-suggestions-trigger").withSchedule(scheduleBuilder).build();
    }
}
