package com.betting.integration.configuration;

import com.betting.integration.betting.job.AbstractBettingProviderJob;
import com.betting.storage.data.messaging.BettingMessageType;
import com.betting.storage.service.messaging.MessagingSubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class MessagingConfig implements ApplicationContextAware {

    private final MessagingSubscriptionService messagingSubscriptionService;

    private final List<JobDetail> jobDetails;
    private ApplicationContext actx;

    @Autowired
    public MessagingConfig(MessagingSubscriptionService messagingSubscriptionService, List<JobDetail> jobDetails) {
        this.messagingSubscriptionService = messagingSubscriptionService;
        this.jobDetails = jobDetails.stream()
                .filter(jd -> AbstractBettingProviderJob.class.isAssignableFrom(jd.getJobClass()))
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void init() {
        log.info("Subscribing to {}", BettingMessageType.NEW_PREDICTION);
        messagingSubscriptionService
                .subscribe(BettingMessageType.NEW_PREDICTION, m -> {
                    jobDetails.parallelStream().forEach(jd -> {
                        try {
                            AbstractBettingProviderJob job = (AbstractBettingProviderJob) jd.getJobClass().newInstance();
                            actx.getAutowireCapableBeanFactory().autowireBean(job);
                            job.executeInternal(null);
                        } catch (Exception e) {
                            log.error("Error processing import triggered by message", e);
                        }
                    });
                });
    }

    @Override
    public void setApplicationContext(ApplicationContext actx) throws BeansException {
        this.actx = actx;
    }
}
