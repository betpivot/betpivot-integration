package com.betting.integration.prediction.job;

import com.betting.storage.data.prediction.csv.PredictionCsvFile;
import com.betting.storage.service.prediction.PredictionStorageService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.BufferedReader;
import java.io.FileReader;

public class PredictionJob extends QuartzJobBean {

    @Autowired
    private PredictionStorageService storage;

    @Autowired
    private PredictionJobConfiguration conf;

    public void setStorage(PredictionStorageService storage) {
        this.storage = storage;
    }

    public void setConf(PredictionJobConfiguration conf) {
        this.conf = conf;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
//        String csvFilePath = conf.getFilePath();
//        String line;
//        PredictionCsvFile csvFile = new PredictionCsvFile(storage);
//        int i = 1;
//        try (BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
//            while ((line = br.readLine()) != null) {
//                csvFile.addLine(i, line.split(","));
//                i++;
//            }
//            storage.saveMatches(csvFile.toDbData());
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new JobExecutionException(e);
//        }

    }

}
