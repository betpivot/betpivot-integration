package com.betting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class IntegrationApplication {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        LOGGER.info("JVM heap size:{}", Runtime.getRuntime().maxMemory());

	}

	public static void main(String[] args) {
		SpringApplication.run(IntegrationApplication.class, args);
	}
}
