package com.betting;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;

@Configuration
@Setter
public class PushNotificationConfig {

    @PostConstruct
    public void initPushNotifications() throws IOException {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(new ClassPathResource("firebaseKey.json").getInputStream()))
                .setDatabaseUrl("https://betpivot-8e9e4.firebaseio.com")
                .build();
        FirebaseApp.initializeApp(options);

    }
}
